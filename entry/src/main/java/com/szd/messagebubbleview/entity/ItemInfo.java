/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.szd.messagebubbleview.entity;

/**
 * ItemInfo
 *
 * @author AnBetter
 * @since 2021-05-19
 */
public class ItemInfo {
    private int iconItem;
    private int textItem;
    private String numItem;
    private boolean isBoom;

    /** 构造
     *
     * @param item1
     * @param item2
     * @param item3
     */
    public ItemInfo(int item1, int item2, String item3) {
        this.iconItem = item1;
        this.textItem = item2;
        this.numItem = item3;
        isBoom = false;
    }

    public boolean isBoom() {
        return isBoom;
    }

    public void setBoom(boolean isMboom) {
        isBoom = isMboom;
    }

    public int getTextItem() {
        return textItem;
    }

    public void setTextItem(int textItem) {
        this.textItem = textItem;
    }

    public String getNumItem() {
        return numItem;
    }

    public void setNumItem(String numItem) {
        this.numItem = numItem;
    }

    public int getIconItem() {
        return iconItem;
    }

    public void setIconItem(int iconItem) {
        this.iconItem = iconItem;
    }
}
