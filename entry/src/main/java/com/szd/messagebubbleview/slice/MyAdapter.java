/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.szd.messagebubbleview.slice;

import com.szd.messagebubbleview.MessageBubbleView;
import com.szd.messagebubbleview.ResourceTable;
import com.szd.messagebubbleview.entity.ItemInfo;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * MyAdapter
 *
 * @author name
 * @since 2021-03-31
 */
public class MyAdapter extends BaseItemProvider {
    private List<ItemInfo> itemInfos;
    private AbilitySlice slice;
    private MyViewHolder holder;
    private MessageBubbleView.TouchListener mTouchListener;

    /** 构造
     *
     * @param data
     * @param slice
     * @param touchListener
     */
    public MyAdapter(List<ItemInfo> data, AbilitySlice slice, MessageBubbleView.TouchListener touchListener) {
        this.itemInfos = data;
        this.slice = slice;
        this.mTouchListener = touchListener;
    }

    @Override
    public int getCount() {
        return itemInfos == null ? 0 : itemInfos.size();
    }

    @Override
    public Object getItem(int position) {
        if (itemInfos != null && position >= 0 && position < itemInfos.size()) {
            return itemInfos.get(position);
        }
        return Collections.emptyList();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component cpt = component;
        if (component == null) {
            cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item, null, false);
            holder = new MyViewHolder(cpt);
            cpt.setTag(holder);
        }
        MyViewHolder holder1 = (MyViewHolder) cpt.getTag();
        if (itemInfos.get(position).isBoom()) {
            holder1.msg.setVisibility(Component.HIDE);
        } else {
            try {
                holder1.msg.init();
            } catch (NotExistException e) {
                e.getMessage();
            } catch (WrongTypeException e) {
                e.getMessage();
            } catch (IOException e) {
                e.getMessage();
            }

            holder.msg.setNumber(itemInfos.get(position).getNumItem());
            holder.msg.setTouchListener(mTouchListener);
            holder.msg.setBoomListener(new MessageBubbleView.BoomListener() {
                @Override
                public void boom() {
                    itemInfos.get(position).setBoom(true);
                }
            });
        }
        ((MyViewHolder)cpt.getTag()).textSetting.setText(itemInfos.get(position).getTextItem());
        ((MyViewHolder)cpt.getTag()).itemImg.setPixelMap(itemInfos.get(position).getIconItem());
        return cpt;
    }

    /**
     * 用于保存列表项中的子组件信息
     *
     * @author name
     * @since 2021-03-31
     */
    public static class MyViewHolder {
        MessageBubbleView msg;
        Text textSetting;
        Image itemImg;

        MyViewHolder(Component component) {
            itemImg = (Image)component.findComponentById(ResourceTable.Id_item_img);
            msg = (MessageBubbleView) component.findComponentById(ResourceTable.Id_itembezierView);
            textSetting = (Text)component.findComponentById(ResourceTable.Id_text_setting);
            msg.setRoot(ListAbilitySlice.rootAbility);
        }
    }
}
