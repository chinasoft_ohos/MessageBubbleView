/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.szd.messagebubbleview.slice;

import com.szd.messagebubbleview.MessageBubbleView;
import com.szd.messagebubbleview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.window.service.WindowManager;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;

/**
 * RoundImageView
 *
 * @author AnBetter
 * @since 2021-03-31
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MainAbilitySlice_TAG"); // 定义日志标签
    MessageBubbleView bezierView;

    /** onStart
     *
     * @param intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        setStatusBarColor();
        bezierView = (MessageBubbleView) findComponentById(ResourceTable.Id_bezierView);
        bezierView.setRoot(findComponentById(ResourceTable.Id_root2));
        bezierView.setNumber("99+");

        bezierView.setOnActionListener(new MessageBubbleView.ActionListener() {
            @Override
            public void onDrag() {
                HiLog.debug(LABEL, "onDrag: ");
            }

            @Override
            public void onDisappear() {
                HiLog.debug(LABEL, "onDisappear: ");
            }

            @Override
            public void onRestore() {
                HiLog.debug(LABEL, "onRestore: ");
            }

            @Override
            public void onMove() {
                HiLog.debug(LABEL, "onMove: ");
            }
        });
        findComponentById(ResourceTable.Id_button).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    bezierView.resetBezierView();
                } catch (NotExistException e) {
                    HiLog.info(LABEL,e.getMessage());
                } catch (WrongTypeException e) {
                    HiLog.info(LABEL,e.getMessage());
                } catch (IOException e) {
                    HiLog.info(LABEL,e.getMessage());
                }
            }
        });
        findComponentById(ResourceTable.Id_button2).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new ListAbilitySlice(), new Intent());
            }
        });
    }

    private void setStatusBarColor() {
        try {
            WindowManager.getInstance().getTopWindow().get().setStatusBarColor(
                    this.getResourceManager().getElement(ResourceTable.Color_status_bar_blue).getColor());
        } catch (IOException e) {
            e.getMessage();
        } catch (NotExistException e) {
            e.getMessage();
        } catch (WrongTypeException e) {
            e.getMessage();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
