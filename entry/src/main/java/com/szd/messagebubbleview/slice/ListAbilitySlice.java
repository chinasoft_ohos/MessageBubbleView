/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.szd.messagebubbleview.slice;

import com.szd.messagebubbleview.MessageBubbleView;
import com.szd.messagebubbleview.ResourceTable;
import com.szd.messagebubbleview.entity.ItemInfo;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * ListAbilitySlice
 *
 * @author AnBetter
 * @since 2021-03-31
 */
public class ListAbilitySlice extends AbilitySlice {
    /**
     * root
     */
    static Component rootAbility;
    Component root;

    /** onStart
     * @param intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_list);
        root = findComponentById(ResourceTable.Id_root);
        setFind(root);
        List<ItemInfo> listItem = new ArrayList<>();
        String[] data = {"20", "30", "50", "40", "99+"};
        for (int index = 0; index < data.length; index++) {
            listItem.add(new ItemInfo(ResourceTable.Media_icon, ResourceTable.String_ItemText,data[index]));
        }

        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        MyAdapter provider = new MyAdapter(listItem, this, new MessageBubbleView.TouchListener() {
            @Override
            public void touchDown() {
                listContainer.setEnabled(false);
            }

            @Override
            public void touchUp() {
                listContainer.setEnabled(true);
            }
        });
        listContainer.setItemProvider(provider);
    }

    private static void setFind(Component mcomponent) {
        rootAbility = mcomponent;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
