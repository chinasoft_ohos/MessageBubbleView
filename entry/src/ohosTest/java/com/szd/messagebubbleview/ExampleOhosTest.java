/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.szd.messagebubbleview;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Assert;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

/**
 * ExampleOhosTest
 *
 * @author AnBetter
 * @since 2021-03-31
 */
public class ExampleOhosTest{
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.szd.messagebubbleview", actualBundleName);
    }

    @Test
    public void add() {
        float res = NumCalcUtil.add(1f,1f);
        Assert.assertEquals(res,2f,2f);
    }

    @Test
    public void subtract() {
        float res = NumCalcUtil.subtract(2f,1f);
        Assert.assertEquals(res,1f,1f);
    }

    @Test
    public void multiply() {
        float res = NumCalcUtil.multiply(2f,2f);
        Assert.assertEquals(res,4f,4f);
    }

    @Test
    public void divide() {
        float res = NumCalcUtil.divide(4f,2f);
        Assert.assertEquals(res,2f,2f);
    }

}

