# MessageBubbleView

#### 项目介绍
- 项目名称：MessageBubbleView
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现MessageBubbleView仿QQ未读消息气泡，可拖动删除，手指按下拖动的时候有一个拖拽的圆这个圆的半径是不会变化的但是位置会随着手指移动，手指按下拖动的时候有一个固定的圆这个圆的是会变化的但是位置不会变化，圆的半径取决于两个圆的距离，两个圆的距离越大圆半径越小，距离越小圆半径越大，两个圆之间实现贝塞尔
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：master分支

#### 效果演示

<img src="gif/MessageDemo.gif"></img>


#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
 ```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:MessageBubbleView:1.0.0')
    ......  
 }
 ```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

组件主要由自定义Component构成。

原开源项目自定义View，现在openharmony项目自定义Component的控件。

XML中添加自定义控件:

```xml
 <com.szd.messagebubbleview.MessageBubbleView
            ohos:id="$+id:bezierView"
            ohos:width="match_content"
            ohos:height="match_content"
            app:circleColor="$color:red"
            app:radius="16vp"
            app:textColor="$color:white"
            app:textNumber="30"
            app:textSize="26fp"
            />
```

自定义控件声明初始化:

```java
        MessageBubbleView bezierView;
        bezierView = (MessageBubbleView) findComponentById(ResourceTable.Id_bezierView);
        bezierView.setRoot(findComponentById(ResourceTable.Id_root2));
        bezierView.setNumber("99+");
```

自定义MessageBubbleView控件，可以添加自定义属性，具体可用属性如下:

| name                    | type      | info                                                   |
|-------------------------|-----------|--------------------------------------------------------|
| circleColor             | color     | set the color of the circle                            |
| radius                  | float     | set the radius of the circle                           |
| textColor               | color     | sets the color of the unread message                   |
| textNumber              | String    | sets the number of unread messages                     |
| textSize                | float     | sets the size of an unread message                     |

代码中提供可调用的方法：
setDisappearPic(): 接受一组int类型的数组。可将需要自定义的消失动画放入数组中传入。
setNumber(): 设置需要显示的未读消息数量。

setOnActionListener(): 操作的监听，其中包括
onDrag()：被拖拽时，且未超出最大可拖拽距离。
onMove()：被拖拽时，已超出最大可拖拽距离。
onDisappear(): 被拖拽的圆消失后。
onRestore()： 被拖拽后又回到原点。

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0
